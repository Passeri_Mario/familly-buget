<?php

    // Format de la valeur monétaire
    $fmt = numfmt_create( 'fr_FR', NumberFormatter::CURRENCY );

    $annee_actuelle = date("Y");

    include("model/sql_dashboard.php");

    // Ajout d'une transaction

    if(isset($_POST['add'])) {

        $transactionDate = $_POST['transactionDate'];

        $amount = $_POST['amount'];

        $idCategory = $_POST['idCategory'];

        $idPaymentmethod = $_POST['idPaymentmethod'];

        $null = null;

        // Requete pour ajouter une transaction

        if($amount > 0 ){
            include("model/sql_traitement_categories.php");
        }

        header('Location: ../dashboard');

    }

    // Modification d'une transaction

    if(isset($_POST['update'])) {

        $transactionId = $_POST['transactionId'];

        $transactionDate= $_POST['transactionDate'];

        $amount = $_POST['amount'];

        $idCategory = $_POST['idCategory'];

        $idPaymentmethod = $_POST['idPaymentmethod'];

        // Requete de modification d'une transaction
        if($amount > 0 ){
            include("model/sql_traitement_categories.php");
        }

        header('Location: ../dashboard');

    }

    // Suppression d'une transaction

    if(isset($_POST['suppr'])){

        $id = $_POST['ID'];

        $dates = $_POST['dates'];

        // Requete de suppression d'une transaction

        include('model/sql_suppression.php');

        header('Location: ../dashboard');

    }

    include('view/dashboard.phtml');

<?php

    // Format de la valeur monétaire
    $fmt = numfmt_create( 'fr_FR', NumberFormatter::CURRENCY );

    $annee_actuelle = date("Y");

    // controle de la variable GET pour les requetes des mois
    if(isset($_POST['query'])){

        $debut_mois =  $_POST['query'];

        $moi = substr($debut_mois, 5,2);

        $annee = substr($debut_mois, 0,4);

        $fin_du_mois = date('Y-m-d',mktime(0,0,0,$moi + 1,0 ,$annee));

        $liste_des_mois = substr($fin_du_mois, 5, 2);

        $array = [
            "01" => "Janvier",
            "02" => "Février",
            "03" => "Mars",
            "04" => "Avril",
            "05" => "Mai",
            "06" => "Juin",
            "07" => "Juillet",
            "08" => "Août",
            "09" => "Septembre",
            "10" => "Octobre",
            "11" => "Novembre",
            "12" => "Décembre",
        ];
    }

    if(isset($_POST['query_annee'])){
        $annee=  $_POST['query_annee'];
    }

    if(isset($_POST['add'])) {

        $debut_mois =  $_POST['query'];

        $transactionDate= $_POST['transactionDate'];

        $amount = $_POST['amount'];

        $idCategory = $_POST['idCategory'];

        $idPaymentmethod = $_POST['idPaymentmethod'];

        $null = null;

        // Requete pour ajouter une transaction

        if($amount > 0 ){
            include("model/sql_traitement_categories.php");
        }

        header('Location: ../conso_mensuelle');

    }

    // Modification d'une transaction

    if(isset($_POST['update'])) {

        $debut_mois =  $_POST['query'];

        $transactionId = $_POST['transactionId'];

        $transactionDate= $_POST['transactionDate'];

        $amount = $_POST['amount'];

        $idCategory = $_POST['idCategory'];

        $idPaymentmethod = $_POST['idPaymentmethod'];

        // Requete de modification d'une transaction

        if($amount > 0 ){
            include("model/sql_traitement_categories.php");
        }

        header('Location: ../conso_mensuelle');

    }

    // Suppression d'une transaction

    if(isset($_POST['suppr'])){

        $debut_mois =  $_POST['query'];

        $id = $_POST['ID'];

        $dates = $_POST['dates'];

        // Requete de suppression d'une transaction

        include('model/sql_suppression.php');

        header('Location: ../conso_mensuelle');

    }

    include("model/sql_conso_mensuelle.php");

    include("view/conso_mensuelle.phtml");
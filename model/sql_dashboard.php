<?php
    // Récupération des transactions
    $reponsecategories = $bdd->prepare
    ('SELECT
               transaction.transactionId,
               transaction.idCategory,
               transaction.transactionDate, 
               transaction.amount, 
               transaction.idPaymentmethod,
               payment_method.name AS PAIEMENT,
               category.categoryId AS CATID,
               category.name AS CATNAME
               FROM transaction
               INNER JOIN category ON transaction.idCategory = category.categoryId
               INNER JOIN payment_method ON transaction.idPaymentmethod = payment_method.payment_methodId
               WHERE transaction.dateSupp IS NULL
               AND transaction.transactionDate >= "'.$annee_actuelle.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee_actuelle.'-12-31 23:59:59"'
    );

    // Récupération des méthodes de paiements
    $depensePayment_method = $bdd->prepare
    ('SELECT * FROM payment_method');

    // Récupération des catégories
    $depenseCategory = $bdd->prepare
    ('SELECT * FROM category');

    // Données des transactions
    $reponsecategories->execute();
    $donneesCategories = $reponsecategories->fetchAll(PDO::FETCH_ASSOC);

    // Liste des méthodes de paiement
    $depensePayment_method->execute();
    $donnesPayment_method = $depensePayment_method->fetchAll(PDO::FETCH_ASSOC);

    // Liste des categories
    $depenseCategory->execute();
    $donnesdepenseCategory = $depenseCategory->fetchAll(PDO::FETCH_ASSOC);

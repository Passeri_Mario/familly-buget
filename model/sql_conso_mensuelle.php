<?php

    //  Récupération pour le calcule des salaires

    $salaireDashboard = $bdd->prepare
    ('SELECT ROUND(SUM(transaction.amount), 2)
               FROM transaction 
               INNER JOIN category ON transaction.idCategory = category.categoryId 
               WHERE category.transactionType = "1"
               AND transaction.dateSupp IS NULL AND transaction.transactionDate >= "'.$debut_mois.'" AND transaction.transactionDate <= "'.$fin_du_mois.'"
               AND transaction.transactionDate >= "'.$annee.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee.'-12-31 23:59:59"'
    );

    //  Récupération pour le calcule des dépenses
    $depenseDashboard = $bdd->prepare
    ('SELECT ROUND(SUM(transaction.amount), 2)
               FROM transaction 
               INNER JOIN category ON transaction.idCategory = category.categoryId 
               WHERE category.transactionType = "-1"
               AND transaction.dateSupp IS NULL AND transaction.transactionDate >= "'.$debut_mois.'" AND transaction.transactionDate <= "'.$fin_du_mois.'"
               AND transaction.transactionDate >= "'.$annee.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee.'-12-31 23:59:59"'
    );

    // Récupération des transactions
    $reponsecategories = $bdd->prepare
    ('SELECT
               transaction.transactionId,
               transaction.idCategory,
               transaction.transactionDate, 
               transaction.amount, 
               transaction.idPaymentmethod,
               payment_method.name AS PAIEMENT,
               category.categoryId AS CATID,
               category.name AS CATNAME
               FROM transaction
               INNER JOIN category ON transaction.idCategory = category.categoryId
               INNER JOIN payment_method ON transaction.idPaymentmethod = payment_method.payment_methodId
               WHERE transaction.dateSupp IS NULL AND transaction.transactionDate >= "'.$debut_mois.'" AND transaction.transactionDate <= "'.$fin_du_mois.'"
               AND transaction.transactionDate >= "'.$annee.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee.'-12-31 23:59:59"'
    );

    // Récupération des méthodes de paiements
    $depensePayment_method = $bdd->prepare
    ('SELECT * FROM payment_method');

    // Récupération des catégories
    $depenseCategory = $bdd->prepare
    ('SELECT * FROM category');

    // Calcul de toutes les rentré d'argent
    $salaireDashboard->execute();
    $donnees = $salaireDashboard->fetchAll(PDO::FETCH_ASSOC);

    // Calcul de toutes les dépenses
    $depenseDashboard->execute();
    $donnees2 = $depenseDashboard->fetchAll(PDO::FETCH_ASSOC);

    // Données des transactions
    $reponsecategories->execute();
    $donneesCategories = $reponsecategories->fetchAll(PDO::FETCH_ASSOC);

    // Liste des méthodes de paiement
    $depensePayment_method->execute();
    $donnesPayment_method = $depensePayment_method->fetchAll(PDO::FETCH_ASSOC);

    // Liste des categories
    $depenseCategory->execute();
    $donnesdepenseCategory = $depenseCategory->fetchAll(PDO::FETCH_ASSOC);
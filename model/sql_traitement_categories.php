<?php

    $insert = $bdd->prepare('INSERT INTO transaction (  
                         transactionId,
                         transactionDate, 
                         amount,
                         idCategory,
                         idPaymentmethod,
                         dateSupp) VALUES(?, ?, ?, ?, ?, ?)');

    $insert->execute(array($null, htmlentities($transactionDate), htmlentities($amount), htmlentities($idCategory), htmlentities($idPaymentmethod), $null));

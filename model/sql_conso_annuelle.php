<?php

//  Récupération pour le calcule des salaires

$salaireDashboard = $bdd->prepare
('SELECT ROUND(SUM(transaction.amount), 2)
               FROM transaction 
               INNER JOIN category ON transaction.idCategory = category.categoryId 
               WHERE category.transactionType = "1"
               AND transaction.dateSupp IS NULL AND transaction.transactionDate >= "'.$annee.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee.'-12-31 23:59:59"'
);

//  Récupération pour le calcule des dépenses
$depenseDashboard = $bdd->prepare
('SELECT ROUND(SUM(transaction.amount), 2)
               FROM transaction 
               INNER JOIN category ON transaction.idCategory = category.categoryId 
               WHERE category.transactionType = "-1"
               AND transaction.dateSupp IS NULL AND transaction.transactionDate >= "'.$annee.'-01-01 00:00:00" AND transaction.transactionDate <= "'.$annee.'-12-31 23:59:59"'
);

// Calcul de toutes les rentré d'argent
$salaireDashboard->execute();
$donnees = $salaireDashboard->fetchAll(PDO::FETCH_ASSOC);

// Calcul de toutes les dépenses
$depenseDashboard->execute();
$donnees2 = $depenseDashboard->fetchAll(PDO::FETCH_ASSOC);


<?php

    $update = $bdd->prepare('UPDATE transaction
                                       SET transactionDate = :transactionDate,
                                           amount = :amount,
                                           idCategory = :idCategory,
                                           idPaymentmethod = :idPaymentmethod
                                       WHERE transactionId = :transactionId');
    $update->execute(array(
        'transactionDate' => $transactionDate,
        'amount' => $amount,
        'idCategory' => $idCategory,
        'idPaymentmethod' => $idPaymentmethod,
        'transactionId' => $transactionId
    ));

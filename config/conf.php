<?php

    header("X-XSS-Protection: 1; mode=block");

    // Statut de l'application

    include('dev.php'); // <-- Version de l'état de l'application [ dev | prod ]

    try {
        $bdd = new PDO("mysql:host=$host_name; dbname=$database; charset=UTF8", $user_name, $password);
    }
    catch (PDOException $e) {
        echo "Erreur!: " . $e->getMessage() . "<br/>";
        die();
    }

    // Mode maintenance

    $Mode_maintenance = false; // <-- Mode maintenance [ true | false ] //
    if($Mode_maintenance === true){
        header("Location: /maintenance");
    }

    // Heure et date du programme avec configuration du timezone

    date_default_timezone_set('Europe/Paris');

    $script_tz = date_default_timezone_get();

    $heure = date("H:i:s");

    $date = date("Y-m-d");

    $datetime = date_create($date);
-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 01 avr. 2021 à 09:50
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dbs1296407`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `categoryId` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `transactionType` tinyint(4) NOT NULL COMMENT '-1 pour Débit, \r\n+1 pour crédit'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`categoryId`, `name`, `transactionType`) VALUES
(1, 'Revenus', 1),
(2, 'Assurance', -1),
(3, 'Electricité', -1),
(4, 'Gaz', -1),
(5, 'Impôts', -1),
(6, 'Internet', -1),
(7, 'Loyer', -1),
(8, 'Mutuelle', -1),
(9, 'Téléphone', -1),
(10, 'Animaux', -1),
(11, 'Cadeaux', -1),
(12, 'Courses', -1),
(13, 'Divers', -1),
(14, 'Essence', -1),
(15, 'Habillement', -1),
(16, 'Loisirs', -1),
(17, 'Maison', -1),
(18, 'Santé', -1),
(19, 'Parking', -1),
(20, 'Péage', -1),
(21, 'Restaurants', -1),
(22, 'Beauté', -1),
(23, 'Transport', -1);

-- --------------------------------------------------------

--
-- Structure de la table `payment_method`
--

CREATE TABLE `payment_method` (
  `payment_methodId` tinyint(4) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `payment_method`
--

INSERT INTO `payment_method` (`payment_methodId`, `name`) VALUES
(1, 'CB'),
(2, 'ESP'),
(3, 'VIR'),
(4, 'CHEQUE');

-- --------------------------------------------------------

--
-- Structure de la table `transaction`
--

CREATE TABLE `transaction` (
  `transactionId` int(11) NOT NULL,
  `transactionDate` datetime NOT NULL,
  `amount` float NOT NULL,
  `idCategory` smallint(6) NOT NULL,
  `idPaymentmethod` tinyint(4) NOT NULL,
  `dateSupp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `transaction`
--

INSERT INTO `transaction` (`transactionId`, `transactionDate`, `amount`, `idCategory`, `idPaymentmethod`, `dateSupp`) VALUES
(1, '2021-01-01 00:00:00', 4000, 1, 3, NULL),
(2, '2021-02-02 12:00:00', 120, 12, 1, NULL),
(3, '2021-02-05 14:30:00', 40, 10, 1, NULL),
(4, '2021-02-05 08:00:00', 600, 7, 4, NULL),
(5, '2021-04-10 10:00:00', 45, 23, 4, NULL),
(6, '2021-02-11 15:00:00', 120, 13, 2, NULL),
(7, '2021-02-15 20:30:00', 35, 21, 1, NULL),
(8, '2021-03-02 09:00:00', 300, 4, 3, NULL),
(9, '2021-03-01 12:00:00', 500, 6, 1, NULL),
(10, '2021-03-01 01:00:00', 300, 16, 2, NULL),
(11, '2021-03-01 01:00:00', 300, 16, 2, NULL),
(12, '2021-03-01 09:00:00', 300, 2, 3, NULL),
(13, '2021-03-10 00:00:00', 1000, 1, 1, '2021-03-10 00:00:00'),
(14, '2021-03-10 00:00:00', 50, 1, 1, '2021-03-10 00:00:00'),
(15, '2021-03-10 00:00:00', 50, 1, 1, '2021-03-10 00:00:00'),
(16, '2021-03-10 00:00:00', 55, 1, 1, '2021-03-11 00:00:00'),
(17, '2021-03-11 00:00:00', 500, 1, 3, '2021-03-11 00:00:00'),
(18, '2021-01-02 00:00:00', 555, 1, 3, '2021-03-11 00:00:00'),
(19, '2021-01-01 00:00:00', 1000, 1, 3, '2021-03-11 00:00:00'),
(20, '2021-01-02 00:00:00', 500, 1, 3, NULL),
(21, '2021-03-01 00:00:00', 1350, 1, 3, NULL),
(22, '2022-03-11 00:00:00', 1000.5, 1, 1, NULL),
(23, '2021-03-16 00:00:00', 10.548, 1, 1, '2021-03-16 00:00:00'),
(24, '2021-03-16 00:00:00', 11.99, 1, 1, '2021-03-16 00:00:00'),
(25, '2021-03-16 00:00:00', 11.99, 1, 1, '2021-03-16 00:00:00'),
(26, '2021-03-16 00:00:00', 11.998, 1, 1, '2021-03-16 00:00:00'),
(27, '2021-03-16 00:00:00', 1399.99, 1, 3, NULL),
(28, '2021-03-17 00:00:00', 9, 1, 1, NULL),
(29, '2021-03-17 00:00:00', 14, 1, 1, NULL),
(30, '2021-03-17 00:00:00', 15, 1, 1, NULL),
(31, '2021-03-17 00:00:00', 15, 1, 1, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Index pour la table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`payment_methodId`);

--
-- Index pour la table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transactionId`),
  ADD KEY `FK_transaction_category` (`idCategory`),
  ADD KEY `FK_transaction_paymentMethod` (`idPaymentmethod`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `payment_methodId` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `FK_transaction_category` FOREIGN KEY (`idCategory`) REFERENCES `category` (`categoryId`),
  ADD CONSTRAINT `FK_transaction_paymentMethod` FOREIGN KEY (`idPaymentmethod`) REFERENCES `payment_method` (`payment_methodId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

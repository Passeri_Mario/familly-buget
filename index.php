<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Voici les routes pour enregistrer des itinéraires Web de l'application.
    | Les routes sont chargées par $_SERVER['REQUEST_URI']
    |
    */

    // Configuration de l'application
    include ('config/conf.php');

    $request = $_SERVER['REQUEST_URI'];

    switch ($request) {

        // 404
        default:
            http_response_code(404);
            require __DIR__ . '/controller/404.php';
            break;

        // mode maintenance
        case '/maintenance':
        http_response_code(503);
            require __DIR__ . '/controller/maintenance.php';
            break;

        // Page d'atterrissage
        case '/' :
        case '/index':
            http_response_code(200);
            require __DIR__ . '/controller/index.php';
            break;

        // Page du dashboard
        case '/dashboard':
            http_response_code(200);
            require __DIR__ . '/controller/dashboard.php';
            break;

        case '/conso_mensuelle':
            http_response_code(200);
            require __DIR__ . '/controller/conso_mensuelle.php';
            break;

        case '/conso_annuelle':
            http_response_code(200);
            require __DIR__ . '/controller/conso_annuelle.php';
            break;
    }

// menu de navigation
$(document).ready(function(){
    $('.sidenav').sidenav();
});

// inputs
$(document).ready(function() {
    $('input#input_text, textarea#textarea2').characterCounter();
});

$(document).ready(function(){
    $('select').formSelect();
});

// modal
$(document).ready(function(){
    $('.modal').modal();
});

// Tooltips
$(document).ready(function(){
    $('.tooltipped').tooltip();
});

// datepicker
$(document).ready(function() {
    $(".datepicker").datepicker({
        firstDay: true,
        format: 'yyyy-mm-dd',
        i18n: {
            months: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"],
            monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jul", "Aoû", "Sep", "Oct", "Nov", "Dem"],
            weekdays: ["Dimanche","Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
            weekdaysShort: ["Dim","Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
            weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
        }
    });
});

// Sécurité format nombre à virgule et conversion de virgule à point
function only_number(champsTexte){
    // remplace une virgule eventuelle en point
    champsTexte.value = champsTexte.value.replace(",",".");
    // empêche de taper (supprime du champs)  tout "non chiffre" et "non point" (ou virgule)
    champsTexte.value = champsTexte.value.replace(/[^\d\.]/,"");
    // limite à deux chiffres après la virgule
    champsTexte.value = champsTexte.value.replace(/(\.\d{2})./,"$1");
    // supprime toute seconde virgule qui pourrait être tapée
    // vu qu'on n'accepte que 2 chiffres après la virgule, si y'a ".4." on supprime le second point
    champsTexte.value = champsTexte.value.replace(/(\.\d)\./,"$1");
}

function format_price(champsTexte){
    // Si y'a pas 2 décimales on les ajoutes
    champsTexte.value = Number(champsTexte.value).toFixed(2);
}

// Imprimer les résultats
let print = document.getElementById("print");
print.addEventListener("click", function (){
    window.print();
})

// Générer un pdf
let pdf = document.getElementById("download");
pdf.addEventListener("click", function (){

    let content = document.getElementById("body");

    var opt = {
        margin:       0,
        filename:     'budget.pdf',
        image:        { type: 'pdf', quality: 1 },
        html2canvas:  { scale: 2},
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' },
        enableLinks: false
    };

    html2pdf(content, opt);
})

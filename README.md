
Sujet SP Application Web Budget en PHP

Objectifs du projet : Assistant Budget

On se propose de développer une application web de gestion et suivi de son budget financier
L'application permettra une saisie facile des opérations bancaires, aussi bien les dépenses que les
recettes, sur une base journalière, au moment de la transaction, en précisant la catégorie, le moyen
de paiement, etc...

Les transactions pourront ensuite être visualisées sur une base mensuelle, annuelle, ou même par
catégorie, moyen de paiement, etc... sous forme de tableau et/ou de graphiques.
L'application web repose bien sûr sur l'utilisation d'une base de données pour enregistrer le détail de
chaque transaction.

Exemple d'une opération bancaire : Montant de 12€, le 31/01/2020, dépense d'alimentation,
payée par carte bancaire.
